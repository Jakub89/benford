FROM python:3.10-slim


ENV WORKON_HOME=~/.virtualenvs
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /usr/src/app

# install environment dependencies
RUN apt-get update &&   \
    apt-get -y install  \
    libpq-dev           \
    gcc                 \
    libffi-dev          \
    git &&              \
    pip install pipenv

# dependency files
COPY Pipfile            \
     Pipfile.lock       \
     ./

# install project dependencies
RUN pipenv install --clear

# application files
COPY app.py ./app.py
COPY templates ./templates

 # entrypoint file
COPY run.sh ./

ENTRYPOINT ["./run.sh"]
