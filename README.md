# Benford's law calculator

## Run application:
```
docker build -t benford_jcz https://gitlab.com/Jakub89/benford.git#main
docker run -d -p 5000:5000 benford_jcz
```
Application should be available on http://0.0.0.0:5000
