import base64
from io import BytesIO

import numpy as np
from benfordslaw import benfordslaw
from flask import Flask, flash, request, redirect, render_template


def add_api(app):
    @app.route('/', methods=['GET', 'POST'])
    def home():
        if request.method == 'GET':
            return render_template('benford.html')
        if request.method == 'POST':
            if 'file' not in request.files:
                flash('No file part')
                return redirect(request.url)
            file = request.files['file']
            if file.filename == '':
                flash('No file selected')
                return redirect(request.url)
            else:
                try:
                    file_byte_data = file.read()
                    file_data = file_byte_data.decode().split('\n')
                except UnicodeDecodeError:
                    flash('Unable to decode file. Did you select correct format?')
                    return redirect(request.url)
                file_data.pop(-1)
                header_row = file_data.pop(0)
                column_title = request.form.get('column')

                try:
                    data_index = get_data_index(header_row, column_title)
                except ValueError:
                    flash(f"Missing '{column_title}' column name")
                    return redirect(request.url)

                try:
                    benford_data = get_benford_data(file_data, data_index)
                except (ValueError, IndexError):
                    flash('Unable to parse data, did you select correct column?')
                    return redirect(request.url)

                context_data = {'amount': len(benford_data)}
                bl = benfordslaw(alpha=float(request.form.get('alpha')), method='chi2')
                bl.fit(np.array(benford_data))

                benford_plot = bl.plot(title='Results')
                buf = BytesIO()
                benford_plot[0].savefig(buf, format="png")
                context_data['data'] = base64.b64encode(buf.getbuffer()).decode("ascii")

                return render_template('benford.html', context_data=context_data)

    def get_data_index(header_row, column_title):
        if ',' in header_row:
            header_row = header_row.split(',')
        elif '\t' in header_row:
            header_row = header_row.split('\t')
        return header_row.index(column_title)

    def get_benford_data(file_data, data_index):
        benford_data = []
        for row in file_data:
            if ',' in row:
                benford_data.append(int(float(row.split(',')[data_index])))
            elif '\t' in row:
                benford_data.append(int(float(row.split('\t')[data_index])))
            else:
                benford_data.append(int(float(row[data_index])))
        return benford_data


def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = 'not so secret key'

    add_api(app)

    return app


if __name__ == '__main__':
    create_app = create_app()
    create_app.run()
else:
    gunicorn_app = create_app()
